package projet.vues;

import projet.vues.outilsvues.EcouteurTerminaison;
import projet.vues.outilsvues.Outils;
import projet.modele.Bille;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

/**
 * Vue dessinant les billes et contenant les 3 boutons de contrôle (arrêt du programme, lancer les billes, arréter les billes)
 */
public class CadreAngryBalls extends Frame implements VueBillard, MouseListener, MouseMotionListener {

    Billard billard;
    public VueBouton lancerBilles, arrêterBilles, arrêterAnimation, aleatoire;
    Panel centre, bas;

    EcouteurTerminaison ecouteurTerminaison;

    public CadreAngryBalls(String titre, String message, Vector<Bille> billes) throws HeadlessException {
        super(titre);
        Outils.place(this, 0.33, 0.33, 0.5, 0.5);
        this.ecouteurTerminaison = new EcouteurTerminaison(this);

        this.centre = new Panel();
        this.add(this.centre, BorderLayout.CENTER);

        this.bas = new Panel();
        this.bas.setBackground(new Color(12, 54, 90));
        this.add(this.bas, BorderLayout.SOUTH);

        this.billard = new Billard(billes);
        this.billard.setBackground(new Color(255,255,240));
        this.add(this.billard);

        this.billard.addMouseListener(this);
        this.billard.addMouseMotionListener(this);

        this.lancerBilles = new VueBouton(new JButton("lancer l'animation"));
        this.bas.add(this.lancerBilles.bouton);

        this.arrêterAnimation = new VueBouton(new JButton("arreter l'animation"));
        this.bas.add(this.arrêterAnimation.bouton);

        this.arrêterBilles = new VueBouton(new JButton("vitesse nulle"));
        this.bas.add(this.arrêterBilles.bouton);

        this.aleatoire = new VueBouton(new JButton("vitesse aleatoire"));
        this.bas.add(this.aleatoire.bouton);
    }

    public double largeurBillard() {
        return this.billard.getWidth();
    }

    public double hauteurBillard() {
        return this.billard.getHeight();
    }

    @Override
    public void miseAJour() {
        this.billard.repaint();
    }

    /* (non-Javadoc)
     * @see exodecorateur.vues.VueBillard#montrer()
     */
    @Override
    public void montrer() {
        this.setVisible(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for(Bille bille : this.billard.billes)
            bille.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for(Bille bille : this.billard.billes)
            bille.mouseReleased(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        for(Bille bille : this.billard.billes)
            bille.mouseDragged(e);
    }

    /* Évènements non gérés */

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {}
}