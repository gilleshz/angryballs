package projet.vues;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;

/**
 * Classe observable pour l'implémentation du DP observer
 */
public class VueBouton extends Observable {

    public JButton bouton;

    public VueBouton(JButton b) {
        bouton = b;
        bouton.addActionListener(e -> {
            setChanged();
            notifyObservers();
        });
    }
}