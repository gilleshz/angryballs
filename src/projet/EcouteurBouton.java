package projet;

import java.util.Observable;
import java.util.Observer;

public abstract class EcouteurBouton implements Observer {

    AnimationBilles animationBilles;

    public EcouteurBouton(AnimationBilles animationBilles) {
        this.animationBilles = animationBilles;
    }

    @Override
    public abstract void update(Observable o, Object arg);
}
