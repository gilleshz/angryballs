package projet;

import java.util.Observable;

public class EcouteurBoutonAleatoire extends EcouteurBouton {

    public EcouteurBoutonAleatoire(AnimationBilles animationBilles) {
        super(animationBilles);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.animationBilles.vitesseAleatoire();
    }
}
