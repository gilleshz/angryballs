package projet.modele;

import mesmaths.geometrie.base.Vecteur;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Vector;

/**
 * Classe abstraire de base des décorateurs de Bille
 */
public abstract class DecorateurBille extends Bille {

    protected Bille billeDecoree;

    public DecorateurBille(Bille billeDecoree)
    {
        this.billeDecoree = billeDecoree;
    }

    public Bille setPosition(Vecteur position) {
        return billeDecoree.setPosition(position);
    }

    public double getRayon() {
        return billeDecoree.getRayon();
    }

    public Vecteur getVitesse() {
        return billeDecoree.getVitesse();
    }

    public Bille setVitesse(Vecteur vitesse) {
        return billeDecoree.setVitesse(vitesse);
    }

    public Vecteur getAccélération() {
        return billeDecoree.getAccélération();
    }

    public Bille setAccélération(Vecteur accélération) {
        return billeDecoree.setAccélération(accélération);
    }

    public int getClef() {
        return billeDecoree.getClef();
    }

    @Override
    public boolean pointDansLaBille(Vecteur point) {
        return billeDecoree.pointDansLaBille(point);
    }

    public String toString() {
        return billeDecoree.toString();
    }

    public double masse() {
        return billeDecoree.masse();
    }

    public void dessine(Graphics g) {
        billeDecoree.dessine(g);
    }

    public void déplacer(double deltaT) {
        billeDecoree.déplacer(deltaT);
    }

    public void gestionAccélération(Vector<Bille> billes) {
        billeDecoree.gestionAccélération(billes);
    }

    @Override
    public Color getColor() {
        return billeDecoree.getColor();
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {
        billeDecoree.handleMouseEvent(e);
    }

    @Override
    public void mousePressed(MouseEvent e) { billeDecoree.mousePressed(e); }

    @Override
    public void mouseReleased(MouseEvent e) { billeDecoree.mouseReleased(e); }

    @Override
    public void mouseDragged(MouseEvent e) { billeDecoree.mouseDragged(e); }

    public Vecteur getPosition() {
        return billeDecoree.getPosition();
    }

    public boolean gestionCollisionBilleBille(Vector<Bille> billes) {
        return billeDecoree.gestionCollisionBilleBille(billes);
    }

    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        billeDecoree.collisionContour(abscisseCoinHautGauche, ordonnéeCoinHautGauche, largeur, hauteur);
    }

    @Override
    public void setImmobile(boolean immobile) {
        billeDecoree.setImmobile(immobile);
    }
}
