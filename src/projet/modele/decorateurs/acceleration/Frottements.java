package projet.modele.decorateurs.acceleration;

import mesmaths.mecanique.MecaniquePoint;
import projet.modele.Bille;

import java.util.Vector;

public class Frottements extends Acceleration {

    public Frottements(Bille billeDecoree) {
        super(billeDecoree);
    }

    @Override
    public void gestionAcc�l�ration(Vector<Bille> billes) {

        billeDecoree.gestionAcc�l�ration(billes); // remise � z�ro du vecteur acc�l�ration
        this.getAcc�l�ration().ajoute(MecaniquePoint.freinageFrottement(this.masse(), this.getVitesse())); // contribution de l'acc�l�ration due au frottement dans l'air
    }
}
