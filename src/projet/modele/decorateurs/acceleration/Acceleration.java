package projet.modele.decorateurs.acceleration;

import projet.modele.Bille;
import projet.modele.DecorateurBille;

import java.util.Vector;

public abstract class Acceleration extends DecorateurBille {

    public Acceleration(Bille billeDecoree) {
        super(billeDecoree);
    }

    /**
     * D�finie pour tous les d�corateurs de type Acceleration, qui ne g�rent pas les collisions.
     */
    @Override
    public void collisionContour(double abscisseCoinHautGauche, double ordonn�eCoinHautGauche, double largeur, double hauteur) {
        billeDecoree.collisionContour(abscisseCoinHautGauche, ordonn�eCoinHautGauche, largeur, hauteur);
    }

    /**
     * A red�finir dans les classes filles pour g�r�r l'acc�l�ration.
     */
    @Override
    public abstract void gestionAcc�l�ration(Vector<Bille> billes);
}
