package projet.modele.decorateurs.acceleration;

import mesmaths.geometrie.base.Vecteur;
import projet.modele.Bille;

import java.util.Vector;

public class Pesanteur extends Acceleration{

    Vecteur pesanteur;

    public Pesanteur(Bille billeDecoree, Vecteur pesanteur) {
        super(billeDecoree);
        this.pesanteur = pesanteur;
    }

    @Override
    public void gestionAcc�l�ration(Vector<Bille> billes) {

        billeDecoree.gestionAcc�l�ration(billes); // remise � z�ro du vecteur acc�l�ration
        this.getAcc�l�ration().ajoute(this.pesanteur); // contribution du champ de pesanteur (par exemple)
    }
}
