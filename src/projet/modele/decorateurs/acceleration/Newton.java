package projet.modele.decorateurs.acceleration;

import projet.modele.Bille;
import projet.modele.OutilsBille;

import java.util.Vector;

public class Newton extends Acceleration {

    public Newton(Bille billeDecoree) {
        super(billeDecoree);
    }

    @Override
    public void gestionAccélération(Vector<Bille> billes) {

        billeDecoree.gestionAccélération(billes);
        this.getAccélération().ajoute(OutilsBille.gestionAccélérationNewton(this, billes));     // contribution de l'accélération due à l'attraction des autres billes
    }
}
