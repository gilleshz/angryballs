package projet.modele.decorateurs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

import projet.modele.Bille;
import projet.modele.DecorateurBille;
import projet.modele.controleurs.Attrapable;
import projet.modele.controleurs.Attrapee;
import projet.modele.controleurs.EtatBille;

public class BillePilotee extends DecorateurBille {

    private EtatBille controleurCourant;
    private Attrapable Attrapable;
    private Attrapee Attrapee;

    public BillePilotee(Bille suivant) {
        super(suivant);
        this.init();
    }

    /**
     * Initialise le graphe des contrôleurs
     */
    private void init() {
        this.Attrapable = new Attrapable(this, null, null);
        this.Attrapee = new Attrapee(this, this.Attrapable, this.Attrapable);
        this.Attrapable.setPrecedent(this.Attrapee);
        this.Attrapable.setSuivant(this.Attrapee);
        this.controleurCourant = this.Attrapable;
    }

    @Override
    public void mousePressed(MouseEvent e) {
        this.controleurCourant.mousePressed(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        this.controleurCourant.mouseReleased(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        this.controleurCourant.mouseDragged(e);
    }

    public EtatBille getControleurCourant() {
        return controleurCourant;
    }

    public void setControleurCourant(EtatBille controleurCourant) {
        this.controleurCourant = controleurCourant;
    }
}