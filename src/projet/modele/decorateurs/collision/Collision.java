package projet.modele.decorateurs.collision;

import projet.modele.Bille;
import projet.modele.DecorateurBille;

import java.util.Vector;

public abstract class Collision extends DecorateurBille {

    public Collision(Bille billeDecoree) {
        super(billeDecoree);
    }

    /**
     * A redéfinir dans les classes filles pour gérer les collisions.
     */
    @Override
    public abstract void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur);

    /**
     * Définie pour tous les décorateurs de type collision, qui ne gèrent pas l'accélération.
     */
    @Override
    public void gestionAccélération(Vector<Bille> billes) {
        billeDecoree.gestionAccélération(billes);
    }
}
