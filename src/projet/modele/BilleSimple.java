package projet.modele;

import mesmaths.cinematique.Cinematique;
import mesmaths.geometrie.base.Geop;
import mesmaths.geometrie.base.Vecteur;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Vector;

public class BilleSimple extends Bille {

    private Vecteur position; // centre de la bille
    private double rayon; // rayon > 0
    private Vecteur vitesse;
    private Vecteur accélération;
    private int clef; // identifiant unique de la bille

    private Color couleur;
    private static int prochaineClef = 0;
    private static double ro = 1;        // masse volumique

    private boolean immobile = false;

    /**
     * @param centre
     * @param rayon
     * @param vitesse
     * @param accélération
     * @param couleur
     */
    protected BilleSimple(Vecteur centre, double rayon, Vecteur vitesse,
                          Vecteur accélération, Color couleur) {
        this.position = centre;
        this.rayon = rayon;
        this.vitesse = vitesse;
        this.accélération = accélération;
        this.couleur = couleur;
        this.clef = BilleSimple.prochaineClef++;
    }

    /**
     * @param position
     * @param rayon
     * @param vitesse
     * @param couleur
     */
    public BilleSimple(Vecteur position, double rayon, Vecteur vitesse, Color couleur) {
        this(position, rayon, vitesse, new Vecteur(), couleur);
    }

    public double masse() {
        return ro * Geop.volumeSphère(rayon);
    }

    /**
     * @param deltaT
     * @inheritDoc
     */
    public void déplacer(double deltaT) {
        if (!this.immobile) {
            Cinematique.mouvementUniformémentAccéléré(this.getPosition(), this.getVitesse(), this.getAccélération(), deltaT);
        }
    }

    /**
     * @param billes
     * @inheritDoc
     */
    public void gestionAccélération(Vector<Bille> billes) {
        this.getAccélération().set(Vecteur.VECTEURNUL);
    }

    /**
     * @param billes
     * @return
     * @inheritDoc
     */
    public boolean gestionCollisionBilleBille(Vector<Bille> billes) {
        return OutilsBille.gestionCollisionBilleBille(this, billes);
    }

    /**
     * @param abscisseCoinHautGauche
     * @param ordonnéeCoinHautGauche
     * @param largeur
     * @param hauteur
     * @return
     * @inheritDoc
     */
    public void collisionContour(double abscisseCoinHautGauche, double ordonnéeCoinHautGauche, double largeur, double hauteur) {
        return;
    }

    public void dessine(Graphics g) {
        int width, height;
        int xMin, yMin;

        xMin = (int) Math.round(position.x - rayon);
        yMin = (int) Math.round(position.y - rayon);

        width = height = 2 * (int) Math.round(rayon);

        g.setColor(couleur);
        g.fillOval(xMin, yMin, width, height);
        //g.setColor(Color.CYAN);
        //g.drawOval(xMin, yMin, width, height);
    }

    public String toString() {
        return "centre = " + position + " rayon = " + rayon + " vitesse = " + vitesse + " accélération = " + accélération + " couleur = " + couleur + "clef = " + clef;
    }

    @Override
    public Vecteur getPosition() {
        return position;
    }

    @Override
    public Bille setPosition(Vecteur position) {
        this.position = position;
        return this;
    }

    @Override
    public double getRayon() {
        return rayon;
    }

    @Override
    public Vecteur getVitesse() {
        return vitesse;
    }

    @Override
    public Bille setVitesse(Vecteur vitesse) {
        this.vitesse = vitesse;
        return this;
    }

    @Override
    public Vecteur getAccélération() {
        return accélération;
    }

    @Override
    public Bille setAccélération(Vecteur accélération) {
        this.accélération = accélération;
        return this;
    }

    @Override
    public boolean pointDansLaBille(Vecteur point) {
        double distance = Math.sqrt(Math.pow(point.x - this.getPosition().x, 2) + Math.pow(point.y - this.getPosition().y, 2));
        return distance < this.getRayon();
    }

    @Override
    public Color getColor() {
        return this.couleur;
    }

    @Override
    public void handleMouseEvent(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public int getClef() {
        return clef;
    }

    public Color getCouleur() {
        return couleur;
    }

    public static int getProchaineClef() {
        return prochaineClef;
    }

    public static double getRo() {
        return ro;
    }

    @Override
    public void setImmobile(boolean immobile) {
        this.immobile = immobile;
    }
}
