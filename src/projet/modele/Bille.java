package projet.modele;

import mesmaths.geometrie.base.Vecteur;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

/**
 * Cas g�n�ral d'une bille de billard
 */
public abstract class Bille implements MouseListener, MouseMotionListener
{
    // Getters
    public abstract Vecteur getPosition();
    public abstract Bille setPosition(Vecteur position);
    public abstract double getRayon();
    public abstract Vecteur getVitesse();
    public abstract Bille setVitesse(Vecteur vitesse);
    public abstract Vecteur getAcc�l�ration();
    public abstract Bille setAcc�l�ration(Vecteur acc�l�ration);
    public abstract int getClef();
    public abstract String toString();
    public abstract boolean pointDansLaBille(Vecteur point);
    public abstract Color getColor();
    public abstract void handleMouseEvent(MouseEvent e);
    public abstract void setImmobile(boolean immobile);

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    @Override
    public abstract void mousePressed(MouseEvent e);

    @Override
    public abstract void mouseReleased(MouseEvent e);

    @Override
    public abstract void mouseDragged(MouseEvent e);

    /**
     * @return masse de la bille
     */
    public abstract double masse();

    /**
     * Dessine la bille
     * @param g
     */
    public abstract void dessine(Graphics g);

    /**
     * mise � jour de position et vitesse � t+deltaT � partir de position et vitesse � l'instant t
     *
     * modifie le vecteur position et le vecteur vitesse
     *
     * laisse le vecteur acc�l�ration intact
     *
     * La bille subit par d�faut un mouvement uniform�ment acc�l�r�
     */
    public abstract void d�placer(double deltaT);

    /**
     * calcul (c-�-d mise � jour) �ventuel  du vecteur acc�l�ration.
     * billes est la liste de toutes les billes en mouvement
     * Cette m�thode peut avoir besoin de "billes" si this subit l'attraction gravitationnelle des autres billes
     * La nature du calcul du vecteur acc�l�ration de la bille  est d�finie dans les classes d�riv�es
     * A ce niveau le vecteur acc�l�ration est mis � z�ro (c'est � dire pas d'acc�l�ration)
     */
    public abstract void gestionAcc�l�ration(Vector<Bille> billes);

    /**
     * gestion de l'�ventuelle  collision de cette bille avec les autres billes
     *
     * billes est la liste de toutes les billes en mouvement
     *
     * Le comportement par d�faut est le choc parfaitement �lastique (c-�-d rebond sans amortissement)
     *
     * @return true si il y a collision et dans ce cas les positions et vecteurs vitesses des 2 billes impliqu�es dans le choc sont modifi�es
     * si renvoie false, il n'y a pas de collision et les billes sont laiss�es intactes
     */
    public abstract boolean gestionCollisionBilleBille(Vector<Bille> billes);

    /**
     * gestion de l'�ventuelle collision de la bille (this) avec le contour rectangulaire de l'�cran d�fini par (abscisseCoinHautGauche, ordonn�eCoinHautGauche, largeur, hauteur)
     *
     * d�tecte si il y a collision et le cas �ch�ant met � jour position et vitesse
     *
     * La nature du comportement de la bille en r�ponse � cette collision est d�finie dans les classes d�riv�es
     */
    public abstract void collisionContour(double abscisseCoinHautGauche, double ordonn�eCoinHautGauche, double largeur, double hauteur);
}

