package projet.modele.controleurs;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import mesmaths.geometrie.base.Vecteur;
import projet.modele.decorateurs.BillePilotee;

public abstract class EtatBille implements MouseListener, MouseMotionListener {

    private EtatBille suivant, precedent;
    protected BillePilotee bille;

    /**
     * Champs statiques car partagés dans l'arborescence.
     */
    static Vecteur positionSouris, positionSourisActuelle, déplacement;
    static Vecteur PositionPrécédente, positionInitiale, vitesseeDeplacement, distanceDeplacement;
    static long datePression, dateCourante, datePrécédente, tempsPassé;
    static double vitesseScalaire;

    public EtatBille(BillePilotee bille, EtatBille suivant, EtatBille precedent) {
        this.bille = bille;
        this.setPrecedent(precedent);
        this.setSuivant(suivant);
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseDragged(MouseEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    public EtatBille getSuivant() {
        return suivant;
    }

    public void setSuivant(EtatBille suivant) {
        this.suivant = suivant;
    }

    public EtatBille getPrecedent() {
        return precedent;
    }

    public void setPrecedent(EtatBille precedent) {
        this.precedent = precedent;
    }

    public BillePilotee getBille() {
        return bille;
    }
}