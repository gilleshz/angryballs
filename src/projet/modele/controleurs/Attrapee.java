package projet.modele.controleurs;

import mesmaths.geometrie.base.Vecteur;
import projet.modele.decorateurs.BillePilotee;

import java.awt.event.MouseEvent;

public class Attrapee extends EtatBille {

    /**
     * @param bille la bille concern�e
     * @param suivant l'�tat suivant dans le graphe
     * @param precedent l'�tat pr�c�dent dans le graphe
     *
     * Ce controleur g�re les events mouseDragged et mouseReleased
     */
    public Attrapee(BillePilotee bille, EtatBille suivant, EtatBille precedent) {
        super(bille, suivant, precedent);
    }

    public void mouseDragged(MouseEvent e) {
        // On r�cup�re les anciennes valeurs de temps et de position
        PositionPr�c�dente = positionSourisActuelle;
        datePr�c�dente = dateCourante;

        // Ainsi que les valeurs courantes
        positionSourisActuelle = new Vecteur(e.getX(), e.getY());
        dateCourante = e.getWhen();

        // on calcule le temps �coul� et la distance parcourue afin d'en d�duire la vitesse
        tempsPass� = dateCourante - datePr�c�dente;
        distanceDeplacement = positionSourisActuelle.difference(PositionPr�c�dente);
        vitesseScalaire = distanceDeplacement.norme() / tempsPass�;

        // Si le temps �coul� est non nul
        if (tempsPass� != 0) {
            vitesseeDeplacement = new Vecteur(
                    distanceDeplacement.produit(
                            vitesseScalaire / distanceDeplacement.norme()));
        }

        // On affecte la nouvelle position � la bille
        bille.setPosition(positionSourisActuelle);

        // Et une vitesse et acc�l�ration nulle afin qu'elle reste en place
        bille.setVitesse(new Vecteur(0, 0));
        bille.setAcc�l�ration(new Vecteur(0, 0));
        bille.setImmobile(true);
    }

    public void mouseReleased(MouseEvent e) {
        bille.setVitesse(vitesseeDeplacement);
        bille.setControleurCourant(getSuivant());
        bille.setImmobile(false);
    }
}