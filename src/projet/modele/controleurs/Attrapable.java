package projet.modele.controleurs;

import mesmaths.geometrie.base.Vecteur;
import projet.modele.Bille;
import projet.modele.decorateurs.BillePilotee;

import java.awt.event.MouseEvent;
import java.util.Vector;

public class Attrapable extends EtatBille {

    /**
     * @param bille la bille concern�e
     * @param suivant l'�tat suivant dans le graphe
     * @param precedent l'�tat pr�c�dent dans la graphe
     *
     * Ce contr�leur g�re l'event mousePressed
     */
    public Attrapable(BillePilotee bille, EtatBille suivant, EtatBille precedent){
        super(bille, suivant, precedent);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        positionSouris = new Vecteur(e.getX(), e.getY()); // Position du clic
        bille = getBille();

        // Si le clic est dans la bille
        if(bille.pointDansLaBille(positionSouris)){

            positionSourisActuelle = positionSouris; // On enregistre la position
            datePression = e.getWhen(); // et la date de la pression du clic
            dateCourante = datePression;
            bille.setVitesse(new Vecteur()); // On r�initialise la vitesse de la bille au vecteur nul
            vitesseeDeplacement = new Vecteur(); // On initialise la vitesse de d�placement de la souris
            positionInitiale = bille.getPosition(); // On enregistre la position initiale

            // calcul de la diff�rence entre la position du clic et le centre de la bille
            d�placement = positionSouris.difference(positionInitiale);

            // Passage au contr�leur suivant
            bille.setControleurCourant(getSuivant());
        }
    }
}