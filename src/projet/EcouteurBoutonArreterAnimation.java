package projet;

import java.util.Observable;

public class EcouteurBoutonArreterAnimation extends EcouteurBouton {

    public EcouteurBoutonArreterAnimation(AnimationBilles animationBilles) {
        super(animationBilles);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.animationBilles.arręterAnimation();
    }
}
