package projet;

import java.util.Observable;

public class EcouteurBoutonLancer extends EcouteurBouton {

    public EcouteurBoutonLancer(AnimationBilles animationBilles) {
        super(animationBilles);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.animationBilles.lancerAnimation();
    }
}
