package projet;

import java.util.Observable;

public class EcouteurBoutonArreterBilles extends EcouteurBouton {

    public EcouteurBoutonArreterBilles(AnimationBilles animationBilles) {
        super(animationBilles);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.animationBilles.arręterBilles();
    }
}
