package projet;

import mesmaths.geometrie.base.Vecteur;
import projet.modele.*;
import projet.modele.decorateurs.BillePilotee;
import projet.modele.decorateurs.acceleration.Frottements;
import projet.modele.decorateurs.acceleration.Newton;
import projet.modele.decorateurs.acceleration.Pesanteur;
import projet.modele.decorateurs.collision.Arret;
import projet.modele.decorateurs.collision.PasseMurailles;
import projet.modele.decorateurs.collision.Rebond;
import projet.vues.CadreAngryBalls;

import java.awt.*;
import java.util.Vector;

/**
 * Gestion d'une liste de billes en mouvement ayant toutes un comportement différent
 * Idéal pour mettre en place le DP decorator
 */
public class TestAngryBalls {

    /**
     * @param args
     */
    public static void main(String[] args) {

        //------------------- création de la liste (pour l'instant vide) des billes -----------------------
        Vector<Bille> billes = new Vector<Bille>();

        //---------------- création de la vue responsable du dessin des billes -------------------------
        CadreAngryBalls cadre = new CadreAngryBalls("Angry balls",
                "Animation de billes ayant des comportements différents. Situation idéale pour mettre en place le DP Decorator",
                billes);

        cadre.montrer(); // on rend visible la vue

        //------------- remplissage de la liste avec 4 billes -------------------------------
        double xMax, yMax;
        double vMax = 0.1;
        xMax = cadre.largeurBillard();      // abscisse maximal
        yMax = cadre.hauteurBillard();      // ordonnée maximale

        double rayon = 0.05 * Math.min(xMax, yMax); // rayon des billes : ici toutes les billes ont le même rayon, mais ce n'est pas obligatoire

        Vecteur p0, p1, p2, p3, p4, v0, v1, v2, v3, v4; // les positions des centres des billes et les vecteurs vitesse au démarrage.
        // Elles vont être choisies aléatoirement

        //------------------- création des vecteurs position des billes ---------------------------------
        p0 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p1 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p2 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p3 = Vecteur.créationAléatoire(0, 0, xMax, yMax);
        p4 = Vecteur.créationAléatoire(0, 0, xMax, yMax);

        //------------------- création des vecteurs vitesse des billes ---------------------------------
        v0 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v1 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, 0);
        v2 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v3 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);
        v4 = Vecteur.créationAléatoire(-vMax, -vMax, vMax, vMax);

        //--------------- ici commence la partie à changer ---------------------------------
        /*billes.add(new BilleMvtRURebond(p0, rayon, v0, Color.red));
        billes.add(new BilleMvtPesanteurFrottementRebond(p1, rayon, v1, new Vecteur(0, 0.001), Color.yellow));
        billes.add(new BilleMvtNewtonFrottementRebond(p2, rayon, v2, Color.green));
        billes.add(new BilleMvtRUPasseMurailles(p3, rayon, v3, Color.cyan));
        billes.add(new BilleMvtNewtonArret(p4, rayon, v4, Color.black));*/

        billes.add( new BillePilotee(new Rebond( new BilleSimple(p0, rayon, v0, new Color(178,34,34)))));
        billes.add( new BillePilotee(new Pesanteur( new Frottements( new Rebond( new BilleSimple(p1, rayon, v1, new Color(255, 227, 29)))),new Vecteur(0,0.001))));
        billes.add( new BillePilotee(new Newton( new Frottements( new Rebond( new BilleSimple(p2, rayon, v2, new Color(0, 148,0)))))));
        billes.add( new BillePilotee(new PasseMurailles( new BilleSimple(p3, rayon, v3, new Color(255,105,180)))));
        billes.add( new BillePilotee(new Newton( new Arret( new BilleSimple(p4, rayon, v4, new Color(43, 43, 211))))));
        //---------------------- ici finit la partie à changer -------------------------------------------------------------

        System.out.println("billes = " + billes);

        //-------------------- création de l'objet responsable de l'animation (c'est un thread séparé) -----------------------
        AnimationBilles animationBilles = new AnimationBilles(billes, cadre);

        //----------------------- mise en place des écouteurs de boutons qui permettent de contrôler (un peu...) l'application -----------------
        EcouteurBoutonLancer écouteurBoutonLancer = new EcouteurBoutonLancer(animationBilles);
        EcouteurBoutonArreterAnimation écouteurBoutonArrêterAnimation = new EcouteurBoutonArreterAnimation(animationBilles);
        EcouteurBoutonArreterBilles écouteurBoutonArrêterBilles = new EcouteurBoutonArreterBilles(animationBilles);
        EcouteurBoutonAleatoire écouteurBoutonAleatoire = new EcouteurBoutonAleatoire(animationBilles);

        //------------------------- activation des écouteurs des boutons et ça tourne tout seul ------------------------------
        //cadre.lancerBilles.addActionListener(écouteurBoutonLancer);             // maladroit : à changer
        //cadre.arrêterBilles.addActionListener(écouteurBoutonArrêter);           // maladroit : à changer
        cadre.lancerBilles.addObserver(écouteurBoutonLancer);
        cadre.arrêterBilles.addObserver(écouteurBoutonArrêterBilles);
        cadre.arrêterAnimation.addObserver(écouteurBoutonArrêterAnimation);
        cadre.aleatoire.addObserver(écouteurBoutonAleatoire);
    }

}
