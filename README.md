# Projet AngryBalls

Projet de M1 Informatique pour l'UE Design Pattern.

## Installation

- Installer IntelliJ IDEA : https://www.jetbrains.com/idea/
- Cloner ce dépôt git
- Ouvrir le projet dans Intellij IDEA
- Lancer `TestAngryBalls.java`

## Tags

Des tags sont placés aux étapes importantes du développement du projet.

- Le tag `clean_start` correspond au code initial "maladroit" du projet, 
ré-indenté et restructuré. 
Les accents sont *malheureusement* gardés car utilisés dans la librairie `mesmaths`. 
Attention à sélectionner le bon encodage (windows-1252).

- Le tag `decorator` correspond à la première étape du projet : la mise en place du
design pattern décorateur. Les modifications sont visibles dans le package
`projet.modele.*`. Deux types de décorateurs sont implémentés : `acceleration` et `collision`.
Les décoreteurs`acceleration` gèrent l'accélération de la bille, et les décorateurs `collision`
gèrent les collisions avec les parois du billard (et non des billes entre elles). 

- Le tag `observer` correspond à la deuxième étape du projet : la mise en place du design
pattern observateur pour remplacer les classes `ActionListener` de la fonction `main()` de 
façon à la rendre indépendante de la vue.